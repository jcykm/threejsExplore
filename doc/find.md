* 可在线预览的 3D 格式文件 3D格式有：dae、stl、x3d、fbx、obj、ply、3ds、abc

* three.js demo


https://tympanus.net/codrops/2016/04/26/the-aviator-animating-basic-3d-scene-threejs/
https://blog.csdn.net/qq_26822029/article/details/82228171


另一种解决方案 http://www.thingjs.com/guide/?m=sample 收费

中文文档 https://threejs.org/docs/#api/zh/core/Object3D

https://github.com/mrdoob/three.js/tree/5fa442d2d4478f6ef1a8697a969abeef92b51507


关于每个机器部件加载

three.js引入3dmax生成的.obj和.mtl以及对引入模型的移动点击变色操作 [ https://blog.csdn.net/fengyinshenlin/article/details/83421177 ]


支持WebGL的浏览器
Google Chrome 9+、Firefox 4+、Opera 15+、Safari 5.1+、Internet Explorer 11 和 Microsoft Edge。你可以点击Can I use WebGL来查阅各个浏览器对WebGL的支持性。


https://blog.csdn.net/u012539364/article/details/74062102/

https://blog.csdn.net/opengl_es/article/details/38166751

https://www.cnblogs.com/CQ-engineer/p/6163359.html

https://www.cnblogs.com/pursues/p/5226807.html


对元素可选中带边框 [ https://github.com/mrdoob/three.js/blob/master/examples/webgl_postprocessing_outline.html ]

webGL 中文网 [ http://www.hewebgl.com/article/articledir/1 ]


http://feg.netease.com/archives/301.html

http://www.hewebgl.com/article/getarticle/127 three.js 文档 内容介绍

Object3D的重要遍历函数traverse，它可以遍历内部的Object3D对象，并给你赋值，实现多纹理，对象的组合和撤分等工作
traverse 示例 [ http://feg.netease.com/archives/301.html ]

学习过程 https://www.cnblogs.com/catherinezyr/p/7047465.html

常用object（对象）介绍

作者：菜鸟教程
链接：https://www.imooc.com/article/20484
来源：慕课网


DemoThreeJ  代码说明 [ https://blog.csdn.net/bigRabbit741/article/details/71191970 ]


鼠标选中模型方法 [ https://blog.csdn.net/qq_30100043/article/details/79054862 ]

通过 THREE.Raycaster 实现模型选中与信息显示 [ https://blog.csdn.net/ithanmang/article/details/80897888 ]

* [ https://blog.csdn.net/qq_36947174/article/details/82843286 ]
* [ https://blog.csdn.net/ithanmang/article/details/80901893 ]
* [ http://feg.netease.com/archives/301.html ]
* [ http://www.wjceo.com/blog/threejs/2018-02-13/60.html ]